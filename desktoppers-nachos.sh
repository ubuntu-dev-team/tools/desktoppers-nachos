#!/bin/bash
set -e
set -x

export PATH=/bin:/sbin:/usr/bin:/usr/sbin

ubuntu_sources=(
    adwaita-icon-theme
    cheese
    file-roller
    gdm3
    gedit
    gjs
    glib2.0
    gnome-builder
    gnome-calculator
    gnome-control-center
    gnome-desktop3
    gnome-disk-utility
    gnome-initial-setup
    gnome-keyring
    gnome-menus
    gnome-mines
    gnome-online-accounts
    gnome-pkg-tools
    gnome-screensaver
    gnome-screenshot
    gnome-session
    gnome-settings-daemon
    gnome-shell
    # gnome-shell-extension-gsconnect
    gnome-software
    gnome-terminal
    gnome-themes-extra
    gsettings-desktop-schemas
    gtk+2.0
    gtk+3.0
    gvfs
    libgweather
    mutter
    nautilus
    pango1.0
    pygobject
    rhythmbox
    # shotwell
    totem
    vala
    vino
    vte2.91
)

# Maybe can get from ci...
# Add CI!

rel=$(ubuntu-distro-info --latest)
if [ "$rel" != "$(lsb_release -cs)" ]; then
    echo "This script is supposed to run in $rel"
    exit 1
fi

sources=${ubuntu_sources[@]}
if [ -n "$*" ]; then
    sources=$*
else
    for s in ${ubuntu_sources[@]}; do
        script -c "$0 $s" $s.log
    done
    exit
fi

function get_ubuntu_branches() {
    exclude="${1:-invalid-branch-name-anyway}"

    git branch | grep -E ' ubuntu' | grep -vw "$exclude" || echo  ''
}

function do_push() {
    dry_run=${1:-dry_run}
    debian_branch="$2"
    shared_branches="$3"
    opt=

    if [[ $debian_branch != *"ubuntu"* ]]; then
        debian_branch=""
    fi

    ubuntu_branches=$(get_ubuntu_branches "$debian_branch")
    ubuntu_tags=$(git tag | grep -E '^ubuntu/' || echo '')
    last_release=$(git describe --abbrev=0)

    if [ "$dry_run" == "dry_run" ]; then
        opt="--dry-run"
    fi

    git push ${opt} salsa $shared_branches $ubuntu_tags $ubuntu_branches $debian_branch
    if [ -n "$debian_branch" ]; then
        git push ${opt} salsa $last_release
    fi
}

for s in $sources; do
    if [ ! -d "$s/.git" ]; then
        pkg_vcs=$(apt showsrc $s | grep-dctrl -s Vcs-Git -n - | head -n1)
        debian_vcs=$(apt showsrc $s | grep-dctrl -s Debian-Vcs-Git -n - | head -n1)

        if [[ $pkg_vcs == *"launchpad"*".git" ]]; then
            echo "Bad desktoppers are bad! :D, Vcs-Git in $s do not properly point to launchpad, fixing it..."
            pkg_vcs=${pkg_vcs/.git}
        fi

        debian_vcs=${debian_vcs% -b*}

        if [[ $pkg_vcs == *"salsa.debian.org"* ]] || [ -z "$pkg_vcs" ]; then
            if apt showsrc $s | grep-dctrl -n -s Version ubuntu -; then
                echo "Bad desktoppers are bad! :D, Vcs-Git in $s do not properly point to launchpad, trying with default"
                debian_vcs="${pkg_vcs}"
                misses_ubuntu_vcs=true
            else
                echo "This package is now debian-only, let's move to salsa for archive reason"
                debian_vcs="$pkg_vcs"
                archive_only=true
            fi

            pkg_vcs="https://git.launchpad.net/~ubuntu-desktop/ubuntu/+source/$s"
        fi

        # pkg_vcs_branch=${pkg_vcs#* -b}
        # [ "$pkg_vcs" == "$pkg_vcs_branch" ] && pkg_vcs_branch=ubuntu/master
        gbp clone --verbose ${pkg_vcs/ -b / --debian-branch } "$s"
        pkg_vcs=${pkg_vcs% -b*}

        debian_vcs=$(echo "$debian_vcs" | sed "s,https://,git@,;s,/,:,")

        if [ -z "$debian_vcs" ]; then
            echo "Bad desktoppers are bad! :D, missing Debian-Vcs-Git in $s, trying with default"
            debian_vcs="git@salsa.debian.org:gnome-team/$s.git"
            touch $s/.nachos-error-misses-debian-vcs
        fi

        if [ "$misses_ubuntu_vcs" == true ]; then
            touch $s/.nachos-error-misses-ubuntu-vcs
        fi

        if [ "$archive_only" == true ]; then
            touch $s/.nachos-archive-only
        fi

        git -C $s remote add salsa "$debian_vcs"
    fi

    echo "$s"
    pushd $s

    debian_branch="$(gbp config DEFAULT.debian-branch)"

    if ! grep "$s" -q $OLDPWD/nachos.checked; then
        upstream_branch="$(gbp config DEFAULT.upstream-branch)"
        ubuntu_vcs_git=$(cat debian/control | grep-dctrl -s Vcs-Git -n -)

        git fetch salsa --tags --force
        git fetch --all --tags || true

        if [ "$(gbp config DEFAULT.pristine-tar)" != 'True' ]; then
            echo "Project $s is not using pristine-tar! Fix it!"
            touch .nachos-error-no-pristine-tar
            false
        fi

        if ! grep -q "native" debian/source/format; then
            shared_branches="$upstream_branch pristine-tar"
        fi

        pull_remote=origin
        if [ -e ".nachos-archive-only" ] ||
           [[ "$ubuntu_vcs_git" == *"salsa"*"ubuntu"* ]]; then
            pull_remote=salsa
        fi

        git pull $pull_remote $debian_branch

        for b in $shared_branches; do
            git fetch $pull_remote $b:$b
        done

        if [ "$pull_remote" != salsa ]; then
            for b in $shared_branches; do
                git fetch salsa $b:$b --force
            done
        fi

        ubuntu_remotes=$(git branch -r | grep -v -- '->' | grep ' origin/ubuntu')
        for r in $ubuntu_remotes; do
            if ! git rev-parse --verify "${r#origin/}"; then
                git branch --track "${r#origin/}" "$r"
            fi
        done

        do_push "dry_run" "$debian_branch" "$shared_branches"

        ubuntu_branches=$(get_ubuntu_branches "$debian_branch")
        if ! [ -e ".nachos-archive-only" ]; then
            ubuntu_branches="$debian_branch $ubuntu_branches"
        fi

        repo_name=$(git show salsa/debian/master:debian/control | grep-dctrl -s Vcs-Git -n - | sed "s,.*/\([^\.]\+\)\.git,\1," || echo $s)

        for branch in $ubuntu_branches; do
            git checkout "$branch"

            debian_vcs_git=$(cat debian/control | grep-dctrl -s XS-Debian-Vcs-Git -n -)
            debian_vcs_git=${debian_vcs_git:-https://salsa.debian.org/gnome-team/$repo_name.git}
            debian_vcs_browser=$(cat debian/control | grep-dctrl -s XS-Debian-Vcs-Browser -n -)
            debian_vcs_browser=${debian_vcs_browser:-https://salsa.debian.org/gnome-team/$repo_name}
            ubuntu_vcs_git=$(cat debian/control | grep-dctrl -s Vcs-Git -n -)
            ubuntu_vcs_browser=$(cat debian/control | grep-dctrl -s Vcs-Browser -n -)
            ubuntu_salsa_vcs_git="${debian_vcs_git% -b*} -b $branch"
            ubuntu_salsa_browser="${debian_vcs_browser}/tree/$branch"

            if [ "$ubuntu_vcs_git" == "$ubuntu_salsa_vcs_git" ] &&
               [ "$ubuntu_vcs_browser" == "$ubuntu_salsa_browser" ]; then
               continue;
            fi

            if [ -z "$ubuntu_vcs_git" ]; then
                touch ".nachos-error-no-vcs-tag-${branch//\//-}"
                continue;
            fi

            if [ -n "$debian_vcs_git" ]; then
                read -r -d '' VCS <<EOF || true
XS-Debian-Vcs-Git: $debian_vcs_git
XS-Debian-Vcs-Browser: $debian_vcs_browser
Vcs-Git: $ubuntu_salsa_vcs_git
Vcs-Browser: $ubuntu_salsa_browser
EOF
            else
                read -r -d '' VCS <<EOF || true
Vcs-Git: $ubuntu_salsa_vcs_git
Vcs-Browser: $ubuntu_salsa_browser
EOF
            fi

            sed 's,.*Vcs-.*:.*,__VCS_INFOS_TO_REPLACE__,g' -i debian/control*
            sed '$!N; /^\(__VCS_INFOS_TO_REPLACE__\)\n\1$/!P; D' -i debian/control*

            for ctrl in debian/control*; do
                awk -v r="$VCS" '{gsub(/^__VCS_INFOS_TO_REPLACE__/,r)}1' $ctrl > $ctrl.tmp
                mv $ctrl.tmp $ctrl
            done

            if [ -n "$(git --no-pager diff debian/control*)" ]; then
                git commit debian/control* -m "debian/control*: update VCS informations to point to ubuntu salsa branch"
            fi
        done

        git checkout "$debian_branch"
        echo "$s" >> $OLDPWD/nachos.checked
    fi

    if ! grep "$s" -q $OLDPWD/nachos.pushed; then
        if ! [ -e .nachos-error-* ]; then
            do_push "real_push" "$debian_branch"
            echo "$s" >> $OLDPWD/nachos.pushed
        fi
    fi

    popd
done
